<?php


namespace App\Exception;


use Throwable;

class ValidationException extends \InvalidArgumentException
{
    /**
     * @var \ArrayAccess|string
     */
    private $data = '';

    public function __construct($data, $code = 0, Throwable $previous = null)
    {
        $this->data = $data;

        parent::__construct("", $code, $previous);
    }

    /**
     * @return \ArrayAccess
     */
    public function getErrors()
    {
        return $this->data;
    }
}