<?php


namespace App\Service\Validation;

use App\Exception\ValidationException;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Класс для обработки входного request на job
 *
 * Class JobValidatorService
 * @package App\Service\Validation
 */
class JobValidatorService
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * JobValidatorService constructor.
     * @param ValidatorInterface $validator
     */
    public function __construct(ValidatorInterface $validator)
    {
       $this->validator = $validator;
    }

    /**
     * Возвращает массив ошибок, в случае если входные данные не соотвествуют правилам валидации,
     * в противном случае вовзращает null
     *
     * @param array $data
     * @return void
     */
    public function validate(array $data): void {
        $errors = $this->validator->validate($data, $this->getRules());

        if(count($errors) > 0){
            throw new ValidationException($errors);
        }
    }

    /**
     * Можно вынести всю логику в абстрактный класс и данный метод сделать
     * абстрактным, но в данном случае входной параметр один.
     * Также можно было бы rules вынести в конфигурационные, а на уровне
     * валидатора все проверять
     *
     * @return array
     */
    protected function getRules(): array{
        return [
            'job' => new Assert\Collection([
                'text' => new Assert\Required(),
                'methods' => new Assert\All(),
            ]),
        ];
    }
}