<?php


namespace App\Service\FilterService;


/**
 * Interface FilterInterface
 * @package App\Service\FilterService
 */
interface FilterInterface
{
    /**
     * Метод, который преобразует строку в соотвествии с правилами
     *
     * @param string $text
     * @return string
     */
    public function filter(string $text): string ;
}