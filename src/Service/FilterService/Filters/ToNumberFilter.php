<?php


namespace App\Service\FilterService\Filters;


use App\Service\FilterService\FilterInterface;

/**
 * Class StripTagsFilter
 * @package App\Service\FilterService\Filters
 */
class ToNumberFilter extends AbstractRegexpFilter implements FilterInterface
{
    /**
     * @return string
     */
    protected function getRegExp(): string
    {
       return '/\D/';
    }
}