<?php


namespace App\Service\FilterService\Filters;


use App\Service\FilterService\FilterInterface;

abstract class AbstractRegexpFilter implements FilterInterface
{
    /**
     * @param string $text
     * @return string
     */
    final public function filter(string $text): string
    {
        return preg_replace($this->getRegExp(), $this->getReplacement(), "");
    }

    /**
     * @return string
     */
    abstract protected function getRegExp(): string ;

    /**
     * @return string
     */
    protected function getReplacement(): string {
        return "";
    }
}