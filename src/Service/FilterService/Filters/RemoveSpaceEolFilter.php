<?php


namespace App\Service\FilterService\Filters;


use App\Service\FilterService\FilterInterface;

class RemoveSpaceEolFilter extends RemoveSpaceFilter implements FilterInterface
{
    /**
     * @return string
     */
    protected function getReplacement(): string
    {
        return PHP_EOL;
    }
}