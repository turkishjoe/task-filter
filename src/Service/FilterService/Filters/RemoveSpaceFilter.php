<?php


namespace App\Service\FilterService\Filters;


use App\Service\FilterService\FilterInterface;

class RemoveSpaceFilter extends AbstractRegexpFilter implements FilterInterface
{
    /**
     * @return string
     */
    final protected function getRegExp(): string
    {
        return '/\s+/';
    }
}