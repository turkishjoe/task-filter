<?php


namespace App\Service\FilterService\Filters;


use App\Service\FilterService\FilterInterface;

/**
 * Class StripTagsFilter
 * @package App\Service\FilterService\Filters
 */
class HtmlSpecialCharsFilter implements FilterInterface
{
    /**
     * @param string $text
     * @return string
     */
    public function filter(string $text): string
    {
        return htmlspecialchars($text);
    }
}