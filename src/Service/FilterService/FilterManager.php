<?php


namespace App\Service\FilterService;

/**
 * Class FilterManager
 * @package App\Service\FilterService
 */
class FilterManager
{
    /**
     * @var FilterFactory
     */
    private $factory;

    /**
     * FilterManager constructor.
     * @param FilterFactory $factory
     */
    public function __construct(FilterFactory $factory)
    {
        $this->factory = $factory;
    }

    /**
     * @param string $text
     * @param array $methods
     * @return string
     */
    public function filter(string $text, array $methods): string{
        foreach ($methods as $method){
            $text = $this->factory->get($method)->filter($text);
        }

        return $text;
    }
}