<?php


namespace App\Service\FilterService;

use App\Exception\ValidationException;
use App\Service\FilterService\Filters\HtmlSpecialCharsFilter;
use App\Service\FilterService\Filters\RemoveSpaceEolFilter;
use App\Service\FilterService\Filters\RemoveSpaceFilter;
use App\Service\FilterService\Filters\RemoveSymbolsFilter;
use App\Service\FilterService\Filters\StripTagsFilter;
use App\Service\FilterService\Filters\ToNumberFilter;
use Psr\Container\ContainerInterface;

/**
 * Class FilterFactory
 * @package App\Service\Filter
 */
class FilterFactory
{
    const KEY_MAPPING = [
        "stripTags" => StripTagsFilter::class,
        "removeSpaces" => RemoveSpaceFilter::class,
        "replaceSpacesToEol"=>RemoveSpaceEolFilter::class,
        "htmlspecialchars"=>HtmlSpecialCharsFilter::class,
        "removeSymbols"=>RemoveSymbolsFilter::class,
        "toNumber"=>ToNumberFilter::class
    ];

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * FilterFactory constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param $key
     * @return FilterInterface
     */
    public function get($key): FilterInterface{
        $mappedKey = self::KEY_MAPPING[$key] ?? null;
        if(!$this->container->has($mappedKey)){
            throw new ValidationException(sprintf("Method %s is not found", $key));
        }

        return $this->container->get($mappedKey);
    }

}