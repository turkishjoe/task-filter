<?php


namespace App\Controller;


use App\Exception\ValidationException;
use App\Service\FilterService\FilterManager;
use App\Service\Validation\JobValidatorService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class JobController extends AbstractController
{
    public function processJob(Request $request, JobValidatorService $jobValidatorService, FilterManager $filterService)
    {
        $inputData = $request->attributes->all();
        $jobValidatorService->validate($inputData);

        return new JsonResponse([
            'text' => $filterService->filter($inputData['job']['text'], $inputData['job']['methods'])
        ]);
    }
}