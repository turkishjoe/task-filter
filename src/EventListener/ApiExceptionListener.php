<?php


namespace App\EventListener;


use App\Exception\ValidationException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

class ApiExceptionListener
{
    public function onKernelException(ExceptionEvent $event)
    {
        $exception = $event->getException();
        $response = new JsonResponse();
        if($exception instanceof ValidationException){
            $response->setStatusCode(Response::HTTP_BAD_REQUEST);
            $response->setData([
               'message'=>$exception->getMessage(),
               'errors'=>$exception->getErrors()
            ]) ;
        } else{
            $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
            $response->setData([
                'message'=>'Bad request'
            ]);
        }

        $event->setResponse($response);
    }
}
